fn main() {
    println!("addition of 1 to 100 without loops: {}", sum(&100));
}

fn sum(n: &i32) -> i32 {
    return if n == &0 {
        0
    } else {
        sum(&(n - 2)) + n
    }
}