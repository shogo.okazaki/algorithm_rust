fn main() {
    let start = 0;
    let end = 100;
    let diff = 2;
    let mut sum_for = 0;

    // for で処理
    for i in start..((end / diff) + 1) {
        sum_for += i * diff;
    }
    println!("Calculation result by for: {}", sum_for);

    // while で処理
    let mut j = start;
    let mut sum_while = 0;
    while !(j >= (end + 1)){
        sum_while += j;
        j += diff;
    }
    println!("Calculation result by while: {}", sum_while);

    // loop で処理
    let mut k = start;
    let mut sum_loop = 0;
    loop {
        sum_loop += k;

        if k >= end {
            break;
        } else {
            k += diff;
        }
    }
    println!("Calculation result by loop: {}", sum_loop);
}