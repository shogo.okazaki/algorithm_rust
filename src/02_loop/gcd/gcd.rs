fn main() {
    let mut i = 1071;
    let mut j = 1029;

    if j == 0 {
        println!("result: {}", i);
    }

    if i < j {
        let _tmp = j;
        j = i;
        i = j;
    }

    // while で処理
    // let mut rem = i % j;
    // while !(rem == 0) {
    //     i = j;
    //     j = rem;
    //     rem = i % j;
    // }

    // loop で処理
    loop {
        let r = i % j;
        if r == 0 {
            break;
        }
        i = j;
        j = r;
    }

    println!("result: {}", j);
}