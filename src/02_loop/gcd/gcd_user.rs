use std::{io, process};

fn main() {

    let mut i: i32;
    println!("Please enter 1st number.");
    let mut n = String::new();
    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    i = n.trim().parse()
        .expect("Please type a number!");
    if i < 1 {
        println!("Please enter an integer greater than 1.");
        process::exit(0);
    }

    let mut j: i32;
    println!("Please enter 2nd number.");
    let mut n = String::new();
    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    j = n.trim().parse()
        .expect("Please type a number!");
    if j < 1 {
        println!("Please enter an integer greater than 1.");
        process::exit(0);
    }

    if j == 0 {
        println!("result: {}", i);
    }

    if i < j {
        let _tmp = j;
        j = i;
        i = j;
    }

    // while で処理
    // let mut rem = i % j;
    // while !(rem == 0) {
    //     i = j;
    //     j = rem;
    //     rem = i % j;
    // }

    // loop で処理
    loop {
        let r = i % j;
        if r == 0 {
            break;
        }
        i = j;
        j = r;
    }

    println!("result: {}", j);
}