use queue::queue::Queue;

fn main() {
    let mut queue = Queue::new();

    queue.enqueue(1);
    println!("data: {}", queue.to_string());

    queue.enqueue(9);
    println!("data: {}", queue.to_string());

    queue.enqueue(7);
    println!("data: {}", queue.to_string());

    println!("remove: {}", queue.dequeue());
    println!("data: {}", queue.to_string());

    println!("remove: {}", queue.dequeue());
    println!("data: {}", queue.to_string());

    queue.enqueue(3);
    println!("data: {}", queue.to_string());

    println!("remove: {}", queue.dequeue());
    println!("data: {}", queue.to_string());

    println!("remove: {}", queue.dequeue());
    println!("data: {}", queue.to_string());

    println!("remove: {}", queue.dequeue());
    println!("data: {}", queue.to_string());

}