pub struct Queue {
    data: [isize; 16],
    top: usize,
    last: usize
}

impl Queue {
    pub fn new() -> Self { Queue { data: [0; 16], top: 0, last: 0 }}

    pub fn enqueue(&mut self, input: isize) {
        // let nextLast = if self.last >= 15 {
        //     15
        // } else {
        //     self.last
        // };

        self.data[self.last % 16] = input;
        self.last += 1;
    }

    pub fn dequeue(&mut self) -> isize {
        return if self.last == self.top {
            -1
        } else {
            return match self.data.get(self.top % 16) {
                None => -1,
                Some(value) => {
                    self.top += 1;
                    *value
                }
            }
        }
    }

    pub fn to_string(&mut self) -> String {
        let mut result = String::new();
        for i in self.top..self.last {
            result = format!("{},{}", result, self.data[i]);
        }
        if result != "".to_string() {
            result.remove(0);
        }
        result
    }
}