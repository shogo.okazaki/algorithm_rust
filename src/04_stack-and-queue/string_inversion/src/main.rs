use std::io;
use string_inversion::stack::StringStack;

fn main() {
    let mut input= String::new();
    loop {
        println!("Please enter a string.");
        let mut str= String::new();
        io::stdin().read_line(&mut str)
            .expect("Failed to read line");
        input = str;
        break;
    }
    let mut stack = StringStack::builder_from_string(&input);

    let input_char = input.chars().collect::<Vec<char>>();
    for ch in input_char.iter() {
        stack.push(*ch);
    }

    loop {
        match stack.pop() {
            Err(_err) => break,
            Ok(val) => {
                print!("{}", val)
            }
        }
    }
}