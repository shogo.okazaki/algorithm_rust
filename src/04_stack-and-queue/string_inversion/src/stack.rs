pub struct StringStack {
    data: Vec<char>,
    top: usize,
}

impl StringStack {

    pub fn builder_from_string(input: &String) -> Self {
        let data = input.chars().collect::<Vec<char>>();
        StringStack{ data, top: 0}
    }

    pub fn push(&mut self, input: char) {
        self.data[self.top] = input;
        self.top += 1;
    }

    pub fn pop(&mut self) -> Result<char, String> {
        return if self.top == 0 {
            Err("No value stored on stack!".to_string())
        } else {
            return match self.data.get(self.top - 1) {
                None =>  Err("No value stored on stack!".to_string()),
                Some(value) => {
                    self.top -= 1;
                    Ok(*value)
                }
            }
        }
    }
}