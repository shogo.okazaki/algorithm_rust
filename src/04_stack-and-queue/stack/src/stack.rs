pub struct Stack {
    data: [isize; 16],
    top: usize,
}

impl Stack {
    pub fn new() -> Self {
        Stack{ data: [0; 16], top: 0 }
    }

    pub fn push(&mut self, input: isize) {
        if self.top + 1 > 16 {
            println!("Cannot store more than Stack limit!");
        } else {
            self.data[self.top] = input;
            self.top += 1;
        }
    }

    pub fn pop(&mut self) -> isize {
        return if self.top == 0 {
            -1
        } else {
            return match self.data.get(self.top - 1) {
                None => -1,
                Some(value) => {
                    if self.top > 0 {
                        self.top -= 1;
                    }
                    *value
                }
            }
        }
    }

    pub fn to_string(&self) -> String {
        let mut result = String::new();
        for i in 0..self.top {
            result = format!("{},{}", result, self.data[i]);
        }
        if result != "" {
            result.remove(0);
        }
        result
    }
}