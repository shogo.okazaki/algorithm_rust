use stack::stack::Stack;

fn main() {
    let mut stack = Stack::new();

    stack.push(1);
    println!("data: {}", stack.to_string());

    stack.push(9);
    println!("data: {}", stack.to_string());

    stack.push(7);
    println!("data: {}", stack.to_string());

    println!("remove: {}", stack.pop());
    println!("data: {}", stack.to_string());

    println!("remove: {}", stack.pop());
    println!("data: {}", stack.to_string());

    stack.push(3);
    println!("data: {}", stack.to_string());

    println!("remove: {}", stack.pop());
    println!("data: {}", stack.to_string());

    println!("remove: {}", stack.pop());
    println!("data: {}", stack.to_string());

    println!("remove: {}", stack.pop());
    println!("data: {}", stack.to_string());
}