use std::io;
use matrix::matrix::MatrixBuilder;

fn main() {

    println!("Calculates the product of an array.");

    println!("Please enter the number of columns in the former term: ");
    let m = get_matrix_size();
    println!("Please enter the number of rows in the former term = the number of columns in the latter term: ");
    let n = get_matrix_size();
    println!("Please enter the number of columns in the latter term: ");
    let l = get_matrix_size();

    println!("\nPlease enter the matrix elements in the previous term.");
    let previous_matrix_elements = get_matrix_elements_by_user_input(m, n);
    println!("\nPlease enter the matrix elements in the latter term.");
    let latter_matrix_elements = get_matrix_elements_by_user_input(n, l);

    let previous_matrix = MatrixBuilder::new()
        .col(m)
        .row(n)
        .nums(previous_matrix_elements)
        .finalize();

    let latter_matrix = MatrixBuilder::new()
        .col(n)
        .row(l)
        .nums(latter_matrix_elements)
        .finalize();

    let product_matrix = match previous_matrix.times(&latter_matrix){
        None => panic!("Failed to compute matrix product!"),
        Some(matrix) => matrix,
    };

    println!("result: \n{}", product_matrix.to_string());
}

fn get_matrix_size() -> usize {
    let mut size: isize;
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)
            .expect("Failed to read line");
        size = match input.trim().parse::<isize>() {
            Ok(size) => size,
            Err(error) => {
                println!("[Warn]Please enter an integer: ");
                continue;
            }
        };
        if size < 1 {
            println!("[Warn]Please enter an integer greater than 1: ");
            continue;
        }
        break;
    }
    size as usize
}

fn get_matrix_elements_by_user_input(col: usize, row: usize) -> Vec<Vec<f64>> {
    let mut result: Vec<Vec<f64>> = Vec::new();
    for j in 0..row {
        let mut cols: Vec<f64> = Vec::new();
        for i in 0..col {
            let num: f64;
            loop {
                println!("[{}, {}]: ", j + 1, i + 1);
                let mut input = String::new();
                io::stdin().read_line(&mut input)
                    .expect("Failed to read line");
                num = match input.trim().parse::<f64>() {
                    Ok(num) => num,
                    Err(error) => {
                        println!("[Warn]Please enter an number: ");
                        continue;
                    }
                };
                break;
            }
            cols.insert(i, num);
        }
        result.insert(j, cols);
    }
    result
}