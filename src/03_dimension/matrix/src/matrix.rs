#[derive(Debug)]
pub struct Matrix {
    col: usize,
    row: usize,
    nums: Vec<Vec<f64>>,
}

impl Matrix {

    pub fn to_string(&self) -> String {

        let mut result = String::new();
        for cols in self.nums.iter() {
            for num in cols {
                result = format!("{}{: >6}", result, format!("{:.2}", num));
            }
            result = format!("{}\n", result);
        };
        result
    }

    pub fn times(&self, latter: &Matrix) -> Option<Matrix> {
        if !self.is_possible_to_multiplication(latter) {
            return None;
        }

        let mut nums: Vec<Vec<f64>> = Vec::new();
        for i in 0..self.row {
            let mut cols: Vec<f64> = Vec::new();
            for j in 0..latter.col {
                let mut value: f64 = 0.0;
                for k in 0..self.col {
                     value += self.nums[i][k] * latter.nums[k][j];
                }
                cols.insert(j, value);
            }
            nums.insert(i, cols);
        }
        return Some(Matrix { col: self.row, row: latter.row, nums: nums.clone() });
    }

    pub fn is_possible_to_multiplication(&self, latter: &Matrix) -> bool {
        return self.col == latter.row;
    }
}

pub struct MatrixBuilder {
    col: usize,
    row: usize,
    nums: Vec<Vec<f64>>,
}

impl MatrixBuilder {
    pub fn new() -> MatrixBuilder {
        MatrixBuilder{ col: 0, row: 0, nums: vec![vec![]] }
    }

    pub fn col(&mut self, size: usize) -> &mut MatrixBuilder {
        self.col = size;
        self
    }

    pub fn row(&mut self, size: usize) -> &mut MatrixBuilder {
        self.row = size;
        self
    }

    pub fn nums(&mut self, nums: Vec<Vec<f64>>) -> &mut MatrixBuilder {
        self.nums = nums;
        self
    }

    pub fn finalize(&mut self) -> Matrix {
        Matrix { col: self.col, row: self.row, nums: self.nums.clone() }
    }
}