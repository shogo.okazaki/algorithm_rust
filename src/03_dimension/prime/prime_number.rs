fn main() {
    let mut prime: Vec<u32> = Vec::new();
    prime.push(2);

    for n in 3..101 {
        let mut is_prime = true;
        for l in prime.iter() {
            if n % l == 0 {
                is_prime = false;
                break;
            }
        }
        if is_prime {
            prime.push(n);
        }
    }

    let mut multiple = 0;
    let mut result = String::new();
    for p in prime.iter() {
        if (p / 10) > multiple {
            result = format!("{}{}", result, "\n");
            multiple += 1;
        }
        result = format!("{}{: >3}", result, p);
    }
    println!("{}", result);
}