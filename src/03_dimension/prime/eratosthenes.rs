fn main() {
    const MAX: usize = 100;
    let mut naturals = [1; MAX];
    let last = (MAX as f64).sqrt().trunc() as usize;

    naturals[0] = 0;
    for i in 2..last {
        if naturals[(i * 1) - 1] == 0 {
            continue;
        }
        for j in 2..(MAX / i + 1) {
            naturals[(i * j) - 1] = 0;
        }
    }

    let mut multiple = 0;
    let mut result = String::new();
    for (i, num) in naturals.iter().enumerate() {
        if num == &1 {
            if ((i + 1) / 10) > multiple {
                result = format!("{}{}", result, "\n");
                multiple += 1;
            }
            result = format!("{}{: >3}", result, (i + 1));
        }
    }

    println!("{}", result);
}