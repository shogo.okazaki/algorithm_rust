use std::io;

fn main() {
    println!("Please enter a number.");

    let mut n = String::new();

    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    let n: isize = n.trim().parse()
        .expect("Please type a number!");

    match n % 15 {
        0 => println!("FizzBuzz"),
        3 => println!("Fizz"),
        5 => println!("Buzz"),
        _ => println!("{}", n),
    };
}

