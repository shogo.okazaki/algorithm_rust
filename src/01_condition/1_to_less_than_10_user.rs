use std::io;

fn main() {
    println!("Please enter a number.");

    let mut n = String::new();

    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    let n: isize = n.trim().parse()
        .expect("Please type a number!");

    if n < 1 {
        println!("範囲より小さい数字です。");
    } else if n > 10 {
        println!("範囲より大きい数字です。");
    } else {
        println!("範囲内の数字です。");
    }
}