use std::io;
use std::cmp;

fn main() {
    println!("Please enter a number.");

    let mut n = String::new();

    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    let n: isize = n.trim().parse()
        .expect("Please type a number!");

    match cmp::max(n, 0)  {
        0 => println!("範囲より小さい数字です。"),
        _ => {
            match cmp::min(n, 11) {
                11 => println!("範囲より大きい数字です。"),
                _ => println!("範囲内の数字です。"),
            }
        }
    }
}