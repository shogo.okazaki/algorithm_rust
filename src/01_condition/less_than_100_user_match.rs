use std::io;
use std::cmp::Ordering;

fn main() {

    println!("Please enter a number.");

    let mut n = String::new();
    io::stdin().read_line(&mut n)
        .expect("Failed to read line");

    let n: usize = n.trim().parse()
        .expect("Please type a number!");

    match n.cmp(&100)  {
        Ordering::Less    => println!("100より小さい数字です。"),
        _ => println!("100以上の数字です。"),
    }
}