use std::io;

fn main() {
    println!("Please enter a number.");

    let mut n = String::new();

    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    let n: isize = n.trim().parse()
        .expect("Please type a number!");

    let mut result = String::new();

    if n % 3 == 0 {
        result += "Fizz";
    }

    if n % 5 == 0 {
        result += "Buzz";
    }

    if result.is_empty() {
        result = n.to_string();
    }

    println!("{}", result);
}

