use std::io;

fn main() {

    println!("Please enter a number.");

    let mut n = String::new();

    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    let n: usize = n.trim().parse()
        .expect("Please type a number!");

    if n < 100 {
        println!("100より小さい数字です。");
    } else {
        println!("100以上の数字です。");
    }
}