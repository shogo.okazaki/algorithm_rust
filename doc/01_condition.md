# 条件分岐

## 基本説明

- 通常のプログラムは、上から下に向かって順次実行されます。
- 条件分岐とは、ある特定の条件に一致する場合のみ処理を実行し、条件に一致しない場合にはその処理をスキップする制御を行う機構です。
- 逆に、ある条件に一致しなかった場合に処理を実行させるという制御も可能です。
- Rust では条件分岐構文として以下の式が用意されています。
    - `if` 式
    - `match` 式
- また、列挙型をマッチさせる場合に便利な `if let` 文と `match let` 文が用意されています。

### `if` 式の基本構文

```rust
if condition {
    [ statements ]
} else if elseifcondition {
    [ elseifstatements ] 
} else {
    [ elsestatement ]
}
```

|指定項目|意味|
|---|---|
|condition|必ず指定します。`true` または `false` に評価されるか、`bool` 型に変換可能なデータ型に評価される式を指定する必要があります。|
|statements|省略可能です。conditionが真（`true`）の場合に実行される処理を記述します。|
|`else if`|condition が偽（`false`）の場合に更に条件式を指定する場合に利用します。`elseif` は `if` に続けて複数回指定可能です。|
|elseifcondition|`elseif` が指定されている場合は必ず指定します。意味は condition と同じです。|
|elseifstatements|省略可能です。elseifcondition が真（`true`）の場合に実行されるステートメントを指定します。|
|`else`|省略可能です。`if` または `else if` にのいずれにも該当しない場合の処理開始位置を示します。|
|elsestatements|省略可能です。condition や elseifcondition の式がどれも真（`true`）でない場合に実行される処理を記述します。|

- `if` 式は `let` 文の右辺に持っていくことが可能です。
    - statements や elseifstatements、elsestatements で最後に評価された値を左辺に返します。
    - 全ての statements の型が異なる場合はコンパイルエラーとなります。

```rust
let condition = true;
let number = if condition {
    5
} else {
    6
};

println!("The value of number is: {}", number); //=> 「The value of number is 5」と出力される
```

### `match` 式

- 作成中

## 問題

### 100 より小さい

- `usize` 型の整数 `natural` を定義し、好きな数字を代入しておきます。
- `natural` が 100 より小さい場合は「100より小さい数字です。」と表示し、それ以外の場合は「100以上の数字です。」と表示するプログラムを作成して下さい。
- 以下のステップに沿って作成してください。
    1. `natural` に 100 より小さい数字を代入し、「100より小さい数字です。」と表示されるプログラムを `if` 式を利用して作成してください。
    2. i. のプログラムを改造し、`natural` に 100 以上の数字を代入し、「100以上の数字です。」と表示されるプログラムを作成してください。
    3. 利用者から数値（`n`）を入力してもらい、入力された数字に応じて「100より小さい数字です。」または「100以上の数字です。」と表示されるプログラムを作成してください。
    4. `match` 式を利用して iii と同様の動作をするプログラムを作成してください。

### 1以上10未満

- `isize` 型の整数 `natural` を定義し、好きな数字を代入しておきます。
    1. `natural` の数字に 1 以上 10 未満の数字を代入し、「決められた範囲内の数字です。」と表示されるプログラムを作成してください。
    2. i のプログラムを改造し、`natural` の数字に 1 未満の数字を代入し、「範囲より小さい数字です。」と表示されるプログラムを作成してください。
    3. ii のプログラムを改造し、`natural` の数字に 10 より大きい数字を代入し、「範囲より大きい数字です。」と表示されるプログラムを作成してください。
    4. iii のプログラムを改造し、利用者から数値（`n`）を入力してもらい、入力された数字に応じて「決められた範囲内の数字です。」または「範囲より小さい数字です。」または「範囲より大きい数字です。」と表示されるプログラムを作成してください。
    5. `match` 式を利用し、iv と同様の動作をするプログラムを作成してください。

### FizzBuzz

- FizzBuzz のルールは以下のとおりです。
    - 3で割り切れるなら"Fizz"と表示する
    - 5で割り切れるなら"Buzz"と表示する
    - 両方で割り切れるなら"FizzBuzz"と表示する
    - 上記 3 点に当てはまらない場合は数字を表示する
1. 利用者から数値（`n`）を入力してもらい、入力された数字に応じて FizzBuzz のルールに従って結果を表示するプログラムを `if` 式を利用して作成してください。
2. `match` 式を利用して 1. と同様の動作をするプログラムを作成してください。
